tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}


//Inicjalizacja zmiennej globalnej(Wykorzystywana jest już istniejąca w projekcie klasa GlobalSymbols, która 
//zawiera hashMap jako pamięć na zmienne)
// | Bez podania instrukcji print akcja drukująca nie jest wykonywana. 
// | Wykonywanie akcji semantycznej drukującej wynik tylko z instrukcją print.
prog    : (^(VAR ID) {createVar($ID.text);} | e=expr | ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());})*;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out/$e2.out;} //Dodanie akcji semantycznych dla odejmowania, mnożenia i dzielenia. Analogicznie do tego jak zostało to zrobione dla dodawania
        | ^(PODST i1=ID   e2=expr) {$out = setValue($i1.text, $e2.out);} //Przypisanie wartości zmiennej globalnej
        | ID                       {$out = getVar($ID.text);} //Odwołanie się do zmiennej globalnej
        | INT                      {$out = getInt($INT.text);}
        ;
