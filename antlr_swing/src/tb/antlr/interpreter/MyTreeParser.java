package tb.antlr.interpreter;

import java.util.HashMap;
import java.util.Map;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {


	protected GlobalSymbols globals = new GlobalSymbols();
	
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

   
    protected void createVar(String text) {
    	globals.newSymbol(text);
    }
    
    
    protected Integer setValue(String text, Integer value) {
    	globals.setSymbol(text, value);
    	return value;
    }
    
  
    protected Integer getVar(String text) {
    	return globals.getSymbol(text);
    }
    
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
